package model;

import java.util.ArrayList;
import java.util.List;

// Represents a savings account having an owner name, interest rate (as a decimal)
// and list of transactions
public class SavingsAccount {
    private String ownerName;
    private double interestRate;
    private List<Transaction> transactions;

    // EFFECTS: constructs a savings account owned by owner name with interestRate
    // and an empty list of transactions
    public SavingsAccount(String ownerName, double interestRate) {
        this.ownerName = ownerName;
        this.interestRate = interestRate;
        transactions = new ArrayList<>();
    }

    public String getOwnerName() {
        return ownerName;
    }

    public double getInterestRate() {
        return interestRate;
    }

    // MODIFIES: this
    // EFFECTS: adds transaction t to this account
    public void addTransaction(Transaction t) {
        transactions.add(t);
    }

    // EFFECTS: prints transactions
    public void printTransactions() {
        System.out.println("Transactions for " + ownerName + "'s savings account");
        for (Transaction t : transactions) {
            System.out.println(t);
        }
        System.out.println("Transactions complete for savings account.");
    }
}

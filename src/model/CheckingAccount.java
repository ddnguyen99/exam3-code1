package model;

import java.util.ArrayList;
import java.util.List;

// Represents a checking account having an owner name and list of transactions
public class CheckingAccount {
    private String ownerName;
    private List<Transaction> transactions;

    // EFFECTS: constructs checking account owned by ownerName with
    // an empty list of transactions
    public CheckingAccount(String ownerName) {
        this.ownerName = ownerName;
        transactions = new ArrayList<>();
    }

    public String getOwnerName() {
        return ownerName;
    }

    // MODIFIES: this
    // EFFECTS: adds transaction t to this account
    public void addTransaction(Transaction t) {
        transactions.add(t);
    }

    // EFFECTS: prints transactions
    public void printTransactions() {
        System.out.println("Transactions for " + ownerName + "'s checking account");
        for (Transaction t : transactions) {
            System.out.println(t);
        }
        System.out.println("Transactions complete for checking account.");
    }
}
